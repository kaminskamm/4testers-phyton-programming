def print_each_student_name_capitalized(list_of_students_first_names):
    for first_name in list_of_students_first_names:
        print(first_name.capitalize())


def print_first_ten_intigers_squared():
    #petla po zakresie od 1 do 10
    for intiger in range(1, 11):
        print(intiger ** 2)


if __name__ == '__main__':
    list_of_students = ["kate", "marek", "toSIa", "nicKi", "stephANie"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_intigers_squared()
