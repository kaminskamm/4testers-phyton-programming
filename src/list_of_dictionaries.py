animal = {
     "kind": "dog",
     "age": 2,
     "male": True
 }
animal2 = {
     "kind": "CAT",
     "age": 5,
     "male": False
 }
animal_kinds = ["dog", "cat", "fish"]

#lista
animals =[
{
    "kind": "dog",
    "age": 2,
    "male": True
 },
{
    "kind": "cat",
    "age": 5,
    "male": False
 },
 {
    "kind": "fish",
    "age": 1,
    "male": False
}
]
print(len(animals))
print(animals[0])
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print('the age of last animal is equal to', last_animal_age)

#podwojne indeksowanie- wyciaganie kilku wartości z listy, slownika
print(animals[0]["male"])
print(animals[1]["kind"])

animals.append(
    {
        "kind": "zebra",
        "age": 7,
        "male": False
    }
)
print(animals)
