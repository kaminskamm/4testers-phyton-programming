import random
import time


def generate_random_number():
    return random.randint(0, 40)

def generate_random_email():
    domain = "example.com"
    names_list = ['james', 'kate', 'mark', 'bill']
    random_name = random.choice(names_list)
    #suffix = random.randint(1, 1_000_000)

    #timestamp od 1 stycznia 1970 jaki czas uplynal w sekundach
    suffix = time.time()
    return f'{random_name}.{suffix}@{domain}'

def generate_random_boolean():
    return random.choice([True, False])
    #return bool(random.randint(0, 1))

def get_dictionary_with_random_peronal_data():
    random_email = generate_random_email()
    random_number = generate_random_number()
    random_boolean = generate_random_boolean()

    return{
        "email": random_email,
        "seniority_years": random_number,
        "female": random_boolean
    }

if __name__ == '__main__':
    print(get_dictionary_with_random_peronal_data())


#new
def generate_list_of_dictionaries_with_random_personal_data(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_with_random_peronal_data())
    return list_of_dictionaries


if __name__ == '__main__':
    print(get_dictionary_with_random_peronal_data())
    print(generate_list_of_dictionaries_with_random_personal_data(10))
