movies = ['Dune', 'Star Wars', 'c', 'd', 'e']

last_movie = movies[-1]
movies.append('f')
movies.append('g')
print(len(movies))
middle_movies_range = movies[2:5]
print(middle_movies_range)

#dodanie wartosci niebedace na liscie pod pozycja 2
movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@example.com']
print(len(emails))
print(emails[0])
print(emails[-1])

friend = {
    "name": "Dawid",
    "age": 24,
    "hobby": "swimming"
}

friend_hobbies = friend ["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")

#cos tu nie dziala z tym football
#friend["hobby"].append("football")
#print(friend)

friend["married"] = True
friend["age"] = 44
print(friend)

