def powitanie(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")

def email_adress_generator(imie, nazwisko):
    imie_new = imie.lower()
    nazwisko_new = nazwisko.lower()
    print(f"{imie_new}.{nazwisko_new}@4testers.pl")



if __name__ == '__main__':
    powitanie ("Kasia", "Szczecin")
    powitanie ("Ania", "Katowice")

    email_adress_generator("Janusz", "Nowak")
    email_adress_generator("Barbara", "Kowalska")
